## [1.0.2](https://gitlab.com/to-be-continuous/pre-commit/compare/1.0.1...1.0.2) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([4b622b5](https://gitlab.com/to-be-continuous/pre-commit/commit/4b622b543038e6577d837cb1c3fdc047e2d38f4f))

## [1.0.1](https://gitlab.com/to-be-continuous/pre-commit/compare/1.0.0...1.0.1) (2024-04-22)


### Bug Fixes

* missing flag for `pre-commit install-hooks` ([7d1220c](https://gitlab.com/to-be-continuous/pre-commit/commit/7d1220ca53a303d31e41c73e3744b1eb68c86099))

# 1.0.0 (2024-04-22)


### Features

* initial template version ([20b68b5](https://gitlab.com/to-be-continuous/pre-commit/commit/20b68b52e0ad367492a3d8935a6aa7bcd834a3c6))
